from unittest import TestCase
import time
from ftdi_serial import Serial
from relay_board import RelayBoard

RELAY_BOARD_SERIAL = 'AG0JGDM1'


class TestRelayBoard(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.serial = Serial(device_serial=RELAY_BOARD_SERIAL)
        cls.relay_board = RelayBoard(cls.serial)

    def test_toggle_all(self):
        self.relay_board.write_outputs(0xff)
        self.assertListEqual(self.relay_board.outputs, [True] * 8)
        time.sleep(1)
        self.relay_board.write_outputs(0x00)
        self.assertListEqual(self.relay_board.outputs, [False] * 8)
        time.sleep(1)

    def test_toggle_output(self):
        self.relay_board.set_output(0, True)
        self.assertEquals(self.relay_board.outputs[0], True)
        time.sleep(1)
        self.relay_board.set_output(0, False)
        self.assertEquals(self.relay_board.outputs[0], False)

