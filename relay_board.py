from typing import List
from ftdi_serial import Serial


class RelayBoard:
    def __init__(self, serial: Serial):
        self.serial = serial
        self.state = 0x00
        # change the FTDI chip into async bit-bang mode, with all outputs
        self.serial.device.ftdi.setBitMode(0xff, 1)

    @property
    def outputs(self) -> List[bool]:
        return [self.get_output(num) for num in range(8)]

    def write_outputs(self, state: int):
        self.state = state
        self.serial.write(bytes([self.state]))

    def set_output(self, output: int, state: bool):
        if state:
            new_state = state | (1 << output)
        else:
            new_state = state & ~(1 << output)

        self.write_outputs(new_state)

    def get_output(self, output: int):
        return self.state & (1 << output) > 0
